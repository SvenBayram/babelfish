﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BabelFish
{
    public partial class VocableTraining : Window
    {
        private Status status = Status.None;
        private LinearGradientBrush linearBrushRed;
        private LinearGradientBrush linearBrushGreen;
        private VocableListStruct vocableListStruct;
        private Random rnd = new Random();
        private bool intoGer;
        private int actualVocableIndex;
        private Vocable actualVocable;
        private string answerText;
        private int trys = 3;

        public VocableTraining()
        {
            InitializeComponent();
            CreateGradientBrushs();
            List<VocableListStruct> vocableLists = FileManager.LoadAll();
            vocableListStruct = vocableLists[rnd.Next(0, vocableLists.Count())];
            Start();
        }

        public VocableTraining(VocableListStruct vocableListStruct)
        {
            InitializeComponent();
            CreateGradientBrushs();
            this.vocableListStruct = vocableListStruct;
            Start();
        }

        private enum Status
        {
            None,
            Wrong,
            Right
        }

        private void Start()
        {
            TbInfo1.Text = vocableListStruct.date.ToShortDateString() + " - " + vocableListStruct.topic;
            BtnCheck.Content = "Check  " + trys.ToString();
            NextVocable();
        }

        private void NextVocable()
        {
            actualVocableIndex = RandomWithWeight();
            actualVocable = vocableListStruct.vocabels[actualVocableIndex];
            TbAnswer.Visibility = Visibility.Hidden;
            BtnCheck.Visibility = Visibility.Visible;
            TbTranslation.Text = "";
            answerText = "";
            trys = 3;
            BtnCheck.Content = "Check  " + trys.ToString();
            status = Status.None;
            ToggleBorderColor();

            if (intoGer)
            {
                TbVocable.Text = actualVocable.English[rnd.Next(0, actualVocable.English.Count)] + "  (" + actualVocable.Type + ")";
                foreach (string item in actualVocable.German)
                {
                    answerText += ", " + item;
                }
            }
            else
            {
                TbVocable.Text = actualVocable.German[rnd.Next(0, actualVocable.German.Count)] + "  (" + actualVocable.Type + ")";
                foreach (string item in actualVocable.English)
                {
                    answerText += ", " + item;
                }
            }
            TbAnswer.Text = answerText.Substring(2);
        }

        private void CheckAnswer()
        {

            if (intoGer)
            {
                foreach (string item in actualVocable.German)
                {
                    if (item == TbTranslation.Text)
                    {
                        status = Status.Right;
                        ToggleBorderColor();
                        BtnCheck.Visibility = Visibility.Hidden;
                        TbAnswer.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        status = Status.Wrong;
                        ToggleBorderColor();

                        if (trys == 1)
                        {
                            TbAnswer.Visibility = Visibility.Visible;
                            BtnCheck.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            BtnCheck.Visibility = Visibility.Visible;
                            trys--;
                            BtnCheck.Content = "Check  " + trys.ToString();
                        }
                    }
                }
            }
            else
            {
                foreach (string item in actualVocable.English)
                {
                    if (item == TbTranslation.Text)
                    {
                        status = Status.Right;
                        ToggleBorderColor();
                        BtnCheck.Visibility = Visibility.Hidden;
                        TbAnswer.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        status = Status.Wrong;
                        ToggleBorderColor();

                        if (trys == 1)
                        {
                            TbAnswer.Visibility = Visibility.Visible;
                            BtnCheck.Visibility = Visibility.Hidden;
                        }
                        else
                        {
                            BtnCheck.Visibility = Visibility.Visible;
                            trys--;
                            BtnCheck.Content = "Check  " + trys.ToString();
                        }
                    }
                }
            }
        }

        private int RandomWithWeight()
        {
            intoGer = rnd.Next(0, 2) == 0 ? true : false;

            //ToDo: Generate a function that returns a pseudo random integer
            //based on the weights from the vocabels in the active list.

            return rnd.Next(0, vocableListStruct.vocabels.Count());
        }

        private void CreateGradientBrushs()
        {
            linearBrushRed = new LinearGradientBrush
            {
                StartPoint = new Point(0.5, 0),
                EndPoint = new Point(0.5, 1)
            };
            GradientStop firstRedGS = new GradientStop
            {
                Color = Colors.Red,
                Offset = 1.0
            };
            GradientStop secondRedGS = new GradientStop
            {
                Color = Colors.White,
                Offset = 0.85
            };
            GradientStop thirdRedGS = new GradientStop
            {
                Color = Colors.White,
                Offset = 0.15
            };
            GradientStop fourthRedGS = new GradientStop
            {
                Color = Colors.Red,
                Offset = 0
            };
            linearBrushRed.GradientStops.Add(firstRedGS);
            linearBrushRed.GradientStops.Add(secondRedGS);
            linearBrushRed.GradientStops.Add(thirdRedGS);
            linearBrushRed.GradientStops.Add(fourthRedGS);
            linearBrushGreen = new LinearGradientBrush
            {
                StartPoint = new Point(0.5, 0),
                EndPoint = new Point(0.5, 1)
            };
            GradientStop firstGreenGS = new GradientStop
            {
                Color = Colors.Green,
                Offset = 1.0
            };
            GradientStop secondGreenGS = new GradientStop
            {
                Color = Colors.White,
                Offset = 0.85
            };
            GradientStop thirdGreenGS = new GradientStop
            {
                Color = Colors.White,
                Offset = 0.15
            };
            GradientStop fourthGreenGS = new GradientStop
            {
                Color = Colors.Green,
                Offset = 0
            };
            linearBrushGreen.GradientStops.Add(firstGreenGS);
            linearBrushGreen.GradientStops.Add(secondGreenGS);
            linearBrushGreen.GradientStops.Add(thirdGreenGS);
            linearBrushGreen.GradientStops.Add(fourthGreenGS);
        }

        private void ToggleBorderColor()
        {
            if (status == Status.None)
            {
                TbTranslation.BorderBrush = Brushes.White;
            }
            if (status == Status.Wrong)
            {
                TbTranslation.BorderBrush = linearBrushRed;
            }
            if (status == Status.Right)
            {
                TbTranslation.BorderBrush = linearBrushGreen;
            }
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            CheckAnswer();
        }
        
        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            NextVocable();
        }

        private void BtnQuit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TbTranslation_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (BtnCheck.Visibility == Visibility.Visible)
                {
                    CheckAnswer();
                }
                else
                {
                    NextVocable();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            FileManager.SaveList(vocableListStruct);
            Application.Current.MainWindow.Show();
        }

    }
}
