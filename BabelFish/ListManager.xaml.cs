﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BabelFish
{
    public partial class ListManager : Window
    {
        private List<VocableListStruct> vocableLists;
        
        public ListManager()
        {
            InitializeComponent();
            vocableLists = FileManager.LoadAll();

            foreach (VocableListStruct item in vocableLists)
            {
                LbVocableLists.Items.Add(String.Format("{0, -12}", item.date.ToShortDateString()) + " - " + String.Format("{0, -20}", item.topic));
            }
        }

        private void LbVocableLists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LbVocables.Items.Clear();
            foreach (Vocable vocable in vocableLists[LbVocableLists.SelectedIndex].vocabels)
            {
                LbVocables.Items.Add("Ger: " + String.Format("{0, -30}", vocable.German[0]) + " " + "Eng: " + String.Format("{0, -30}", vocable.English[0]) + " " + "Type: " + String.Format("{0, -6}", vocable.Type) + " " + "Weight: " + String.Format("{0, -6}", vocable.Weight.ToString()));
            }
        }

        private void BtnChoose_Click(object sender, RoutedEventArgs e)
        {
            if (!(LbVocableLists.SelectedIndex == -1))
            {
                VocableTraining vocableTraining = new VocableTraining(vocableLists[LbVocableLists.SelectedIndex]);
                vocableTraining.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Error: You have to select a list!", "No list selected!");
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            FileManager.SaveAll(vocableLists);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Application.Current.MainWindow.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            FileManager.SaveAll(vocableLists);
        }
    }
}
