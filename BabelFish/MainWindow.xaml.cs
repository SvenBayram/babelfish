﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace BabelFish
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Vocable> Vocables = new List<Vocable>();
        private DispatcherTimer PassingTimer = new DispatcherTimer();
        private Random rnd = new Random();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnList_Click(object sender, RoutedEventArgs e)
        {
            ListManager listManager = new ListManager();
            Application.Current.MainWindow.Hide();
            listManager.Show();
        }

        private void BtnTraining_Click(object sender, RoutedEventArgs e)
        {
            VocableTraining vocableTraining = new VocableTraining();
            Application.Current.MainWindow.Hide();
            vocableTraining.Show();
        }

        private void BtnQuit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
