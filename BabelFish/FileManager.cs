﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BabelFish
{
    static class FileManager
    {
        static string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\VocableLists";

        public static List<VocableListStruct> LoadAll()
        {
            List<VocableListStruct> vocableLists = new List<VocableListStruct>();
            foreach (FileInfo file in (new DirectoryInfo(path)).GetFiles())
            {
                vocableLists.Add(LoadList(file.Name));
            }
            return vocableLists;
        }
        
        private static VocableListStruct LoadList(string fileName)
        {
            VocableListStruct vocableListStruct = new VocableListStruct();
            List<Vocable> vocableList = new List<Vocable>();

            XmlDocument doc = new XmlDocument();
            doc.Load(path + @"\" + fileName);
            XmlNodeList vocableNodes = doc.DocumentElement.SelectNodes("/vocable_list/vocable");
            XmlNode infoNode = doc.DocumentElement.SelectSingleNode("/vocable_list/informations");

            foreach (XmlNode node in vocableNodes)
            {
                Vocable vocable = new Vocable();
                foreach (XmlNode childnode in node.ChildNodes)
                {
                    switch (childnode.Name)
                    {
                        case "english":
                            vocable.English.Add(childnode.InnerText);
                            break;
                        case "german":
                            vocable.German.Add(childnode.InnerText);
                            break;
                        case "weight":
                            vocable.Weight = Convert.ToDouble(childnode.InnerText);
                            break;
                        case "type":
                            vocable.Type = childnode.InnerText;
                            break;
                        default:
                            Console.WriteLine("Childnode not found. Path: " + childnode.BaseURI);
                            break;
                    }
                }
                vocableList.Add(vocable);
            }
            foreach (XmlNode childnode in infoNode.ChildNodes)
            {
                switch (childnode.Name)
                {
                    case "date":
                        vocableListStruct.date = Convert.ToDateTime(childnode.InnerText);
                        break;
                    case "topic":
                        vocableListStruct.topic = childnode.InnerText;
                        break;
                    default:
                        Console.WriteLine("Childnode not found. Path: " + childnode.BaseURI);
                        break;
                }
            }

            vocableListStruct.vocabels = vocableList;
            vocableListStruct.fileName = fileName;

            return vocableListStruct;
        }

        public static void SaveAll(List<VocableListStruct> vocableList)
        {
            foreach (VocableListStruct vocableListStruckt in vocableList)
            {
                SaveList(vocableListStruckt);
            }
        }

        public static void SaveList(VocableListStruct vocableListStruct)
        {
            XmlWriter xmlWriter = XmlWriter.Create(@"VocableLists\" + vocableListStruct.fileName);

            xmlWriter.WriteStartDocument();
            {
                xmlWriter.WriteStartElement("vocable_list");
                {
                    xmlWriter.WriteStartElement("informations");
                    xmlWriter.WriteStartElement("date");
                    xmlWriter.WriteString(vocableListStruct.date.ToShortDateString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("topic");
                    xmlWriter.WriteString(vocableListStruct.topic);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                }
            
                foreach (Vocable vocable in vocableListStruct.vocabels)
                {
                    {
                        xmlWriter.WriteStartElement("vocable");

                        foreach (string english in vocable.English)
                        {
                            xmlWriter.WriteStartElement("english");
                            xmlWriter.WriteString(english);
                            xmlWriter.WriteEndElement();
                        }

                        foreach (string german in vocable.German)
                        {
                            xmlWriter.WriteStartElement("german");
                            xmlWriter.WriteString(german);
                            xmlWriter.WriteEndElement();
                        }

                        {
                            xmlWriter.WriteStartElement("weight");
                            xmlWriter.WriteString(vocable.Weight.ToString());
                            xmlWriter.WriteEndElement();
                        }

                        {
                            xmlWriter.WriteStartElement("type");
                            xmlWriter.WriteString(vocable.Type);
                            xmlWriter.WriteEndElement();
                        }

                        xmlWriter.WriteEndElement();
                    }
                }

                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndDocument();

            xmlWriter.Close();
        }
    }
}
