﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabelFish
{
    public struct VocableListStruct
    {
        public string fileName;
        public List<Vocable> vocabels;
        public DateTime date;
        public string topic;
    }

    public class Vocable
    {
        private List<string> english = new List<string>();
        private List<string> german = new List<string>();
        private string type;
        private double weight;

        public Vocable()
        {
            weight = 1.0d;
        }

        public string Type { get => type; set => type = value; }
        public double Weight { get => weight; set => weight = value; }
        public List<string> English { get => english; set => english = value; }
        public List<string> German { get => german; set => german = value; }

        public void DecreaseWeight()
        {

        }

        public void IncreaseWeight()
        {

        }
    }
}
